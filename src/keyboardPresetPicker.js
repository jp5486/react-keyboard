import React, { useContext } from "react";
import { keyboardContext } from "./keyboardContext";

const backgroundArray = ["spacegrey", "lavender", "cherry", "white"];
const keysetArray = ["drifter", "pilot", "atlantis", "lich", "graen",
  "lotus", "pastel-lavender", "pastel-mint", "pastel-peach", "pastel-sakura"];

const formatCandybarCSS = (location) => {
  return location === "background" ? "candybar" : location;
};

export default function KeyboardPresetPicker() {
  const {
    values: { keyboardState },
    actions: { setKeyboardState },
  } = useContext(keyboardContext);

  const clickPresetButtonTwo = (value, location) => () => {
    setKeyboardState({ ...keyboardState, [location]: value });
  };

  const currentSelection = (item, location) => {
    return item === keyboardState[location]
      ? `${formatCandybarCSS(location)}-current`
      : "";
  };

  const formatLists = (list, category) => {
    return list.map((item) => (
      <li
        onClick={clickPresetButtonTwo(item, category)}
        className={`${formatCandybarCSS(
          category
        )} select-${item} ${currentSelection(item, category)}`}
        key={`${category}-${item}`}
      >
        {item}
      </li>
    ));
  };

  return (
    <div>
      <div className="options">
        <fieldset className="candybar">
          <legend>Keyboard</legend>
          <ul className="candybar-list">
            {formatLists(backgroundArray, "background")}
          </ul>
        </fieldset>
        <fieldset className="keyset">
          <legend>Keycaps</legend>
          <ul className="keyset-list">{formatLists(keysetArray, "keyset")}</ul>
        </fieldset>
      </div>
    </div>
  );
}
