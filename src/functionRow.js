import React from "react";
import ChangeKeyBackground from "./changeKeyBackground";

const rowOne = ["F1", "F2", "F3"];
const rowTwo = ["F5", "F6", "F7"];
const rowThree = ["F9", "F10", "F11", "F12"];

export default function FunctionRow() {
  const formatList = (list, size) => {
    return list.map((value) => formatAnyKey(value, size));
  };

  const formatAnyKey = (value, size) => {
    switch (size) {
      case "nav":
        return (
          <div
            onClick={ChangeKeyBackground("nav")}
            key={value}
            className="key modifier"
          >
            <span className="legend">{value}</span>
          </div>
        );
      case "fnBlocker":
        return (
          <div
            onClick={ChangeKeyBackground("fnBlocker")}
            key={value}
            className="key modifier key-fn-blocker"
          >
            <span className="legend">{value}</span>
          </div>
        );
      case "ESC":
        return (
          <div
            onClick={ChangeKeyBackground("ESC")}
            key={value}
            className="key modifier key-esc accent"
          >
            {" "}
            <span className="legend">{value}</span>
          </div>
        );
      default:
        return;
    }
  };

  return (
    <section className="function-row">
      <section className="row row-1">
        {formatAnyKey("ESC", "ESC")}
        {formatList(rowOne, "nav")}
        {formatAnyKey("F4", "fnBlocker")}
        {formatList(rowTwo, "nav")}
        {formatAnyKey("F8", "fnBlocker")}
        {formatList(rowThree, "nav")}
      </section>
    </section>
  );
}
