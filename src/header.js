import React from "react";

function header() {
  return (
    <header>
      <h1>React Keyboard </h1>

      <nav>
        <ul className="nav-list">
          <li className="nav-current">Full-size (100%)</li>
          {/* <li><a href="">1800</a></li>
          <li><a href="">TKL</a></li>
          <li><a href="">75%</a></li>
          <li><a href="">65%</a></li>
          <li><a href="">60%</a></li>
          <li><a href="">OrthoLinear</a></li>
          <li><a href="">CandyBar</a></li> */}
        </ul>
      </nav>
    </header>
  );
}

export default header;
