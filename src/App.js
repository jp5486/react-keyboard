import React from "react";
import Keyboard from "./keyboard";
import PaintBucket from "./paintBucket";
import Header from "./header";
import KeyboardPresetPicker from "./keyboardPresetPicker";
import KeyboardContextProvider from "./keyboardContext";

export default function App() {
  return (
    <div>
      <KeyboardContextProvider>
        <Header />
        <KeyboardPresetPicker />
        <Keyboard />
        <PaintBucket />
      </KeyboardContextProvider>
    </div>
  );
}
