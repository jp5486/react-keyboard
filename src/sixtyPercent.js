import React from "react";
import ChangeKeyBackground from "./changeKeyBackground";

const rowOne = ["~", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "-", "="]
const rowTwo = ["Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P", "[", "]"]
const rowThree = ["A", "S", "D", "F", "G", "H", "J", "K", "L", ";", `'`]
const rowFour = ["Z", "X", "C", "V", "B", "N", "M", "{", "}", "?"]
const leftMods = ["Ctrl", "Code", "Alt"]
const rightMods = ["Code", "Fn", "Alt", "Ctrl"]

export default function SixtyPercent() {

  const formatList = (list, size) => {
    return list.map((value) => formatAnyKey( value, size))
  }

  const formatAnyKey = (value, size) => {
    switch (size) {
      case "1":
        return (<div onClick={ChangeKeyBackground("1")} key={value} className="key"><span className="legend">{value}</span></div>)
      default:
        return (<div onClick={ChangeKeyBackground(size)} key={`${value}-${size}`} className={`key key-${size}u modifier`}><span className="legend">{value}</span></div>)
    }
  }

  return (
    <section className="keyboard-60">
      <section className="row row-1">
        {formatList(rowOne, "1")}
        <div onClick={ChangeKeyBackground("backspace")} className="key key-2u modifier backspace"><span className="legend">Backspace</span></div>
      </section>

      <section className="row row-1">
        <div onClick={ChangeKeyBackground("tab")} className="key key-15u modifier"><span className="legend">Tab</span></div>
        {formatList(rowTwo, "1")}
        <div onClick={ChangeKeyBackground("/")} className="key key-15u"><span className="legend">\</span></div>
      </section>

      <section className="row row-2">
        <div onClick={ChangeKeyBackground("capslock")} className="key key-175u modifier"><span className="legend">Capslock </span></div>
        {formatList(rowThree, "1")}
        <div onClick={ChangeKeyBackground("enter")} className="key key-225u modifier enter accent"><span className="legend">Enter</span></div>
      </section>

      <section className="row row-3">
        <div onClick={ChangeKeyBackground("leftShift")} className="key key-225u modifier"><span className="legend">Shift </span></div>
        {formatList(rowFour, "1")}
        <div onClick={ChangeKeyBackground("rightShift")} className="key key-275u modifier"><span className="legend">Shift </span></div>
      </section>

      <section className="row row-4">
        {formatList(leftMods, "125")}
        <div onClick={ChangeKeyBackground("spacebar")} className="key key-625u spacebar-left"><span className="legend"> </span></div>
        {formatList(rightMods, "125")}
      </section>
    </section>
  )
}
