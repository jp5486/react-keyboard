import React, { useContext } from "react";
import { keyboardContext } from "./keyboardContext";

export default function PaintBucket() {
  const { actions, values } = useContext(keyboardContext);

  const currentSelection = (color, keyset) => {
    let fullIdentifier = `${values.keycapColorState["keysetName"]}-${values.keycapColorState["colorName"]}`;
    return fullIdentifier === `${keyset}-${color}` ? "paint-current" : "";
  };

  const handlePaintClick = (color, keyset) => {
    const handleOnClick = () => {
      actions.setKeycapColorState({
        colorName: `${color}`,
        keysetName: `${keyset}`,
      });
    };
    return handleOnClick;
  };

  const formatKeysetList = (colorList, keyset) => {
    let listItems = colorList.map((color) => (
      <li
        onClick={handlePaintClick(color, keyset)}
        className={`paint-${keyset}-${color} ${currentSelection(
          color,
          keyset
        )}`}
        key={`${keyset}-${color}`}
      >
        {color}
      </li>
    ));

    return <ul className={`paint-list paint-list-${keyset}`}>{listItems}</ul>;
  };

  return (
    <div>
      <section className="options">
        <div className="paint-bucket">
          <p>
            <strong>Paint Bucket</strong>
          </p>
          {formatKeysetList(["red", "teal", "white", "grey"], "drifter")}
          {formatKeysetList(["aqua", "navy"], "atlantis")}
          {formatKeysetList(["lilac", "plum", "yellow"], "lich")}
          {formatKeysetList(["green", "red", "beige"], "graen")}
          {formatKeysetList(["black", "purple", "lilac"], "lotus")}
          {formatKeysetList(
            ["lavender", "mint", "peach", "sakura", "white"],
            "pastel"
          )}
        </div>
      </section>
    </div>
  );
}
