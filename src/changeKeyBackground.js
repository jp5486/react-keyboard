import { useContext } from "react";
import { keyboardContext } from "./keyboardContext";

function getBaseClassName(keySize) {
  const baseClassNameMap = {
    "1": "key", //1ukeys
    "125": "key key-125u modifier", //60% mods
    "nav": "key modifier", // all nav cluster except up arrow & most fkeys
    "upArrow": "key modifier key-up", //up arrow
    "fnBlocker": "key modifier key-fn-blocker", //f4&f8
    "ESC": "key modifier key-esc accent", // esc
    "zero": "key zero", // numpad Zeros
    "/": "key key-15u", // special mods on 60% below
    "backspace": "key key-2u modifier backspace",
    "enter": "key key-225u modifier enter accent",
    "tab": "key key-15u modifier",
    "capslock": "key key-175u modifier",
    "leftShift": "key key-225u modifier",
    "rightShift": "key key-275u modifier",
    "spacebar": "key key-625u spacebar-left"
  };

  return `${baseClassNameMap[keySize]}`;
}

export default function ChangeKeyBackground(keySize) {
  const {
    values: { keycapColorState },
  } = useContext(keyboardContext);
  let colorToAdd = `${keycapColorState["keysetName"]}-${keycapColorState["colorName"]}`;

  const handleOnClick = (event) => {
    let targetClassName = event.currentTarget.className;
    let wasPainted = `${getBaseClassName(keySize)} ${colorToAdd}`;

    event.currentTarget.className =
      targetClassName === wasPainted
        ? `${getBaseClassName(keySize)}`
        : wasPainted;
  };
  return handleOnClick;
}
