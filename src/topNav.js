import React from "react";
import ChangeKeyBackground from "./changeKeyBackground";

const rowOne = ["Prt Sc", "Scroll Lock", "Pause"];

export default function TopNav() {
  const formatList = (list, size) => {
    return list.map((value) => formatAnyKey(value, size));
  };

  const formatAnyKey = (value, size) => {
    return (
      <div
        onClick={ChangeKeyBackground("nav")}
        key={value}
        className="key modifier"
      >
        <span className="legend">{value}</span>
      </div>
    );
  };

  return (
    <section className="navigation">
      <div className="row row-1">{formatList(rowOne, "1")}</div>
    </section>
  );
}
