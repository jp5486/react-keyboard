import React, { useContext } from "react";
import Numpad from "./numpad.js";
import Navigation from "./navigation.js";
import FunctionRow from "./functionRow.js";
import SixtyPecent from "./sixtyPercent.js";
import TopNav from "./topNav.js";
import "./css/keyStyle.css";
import { keyboardContext } from "./keyboardContext";

export default function Keyboard() {
  const {
    values: { keyboardState },
  } = useContext(keyboardContext);

  return (
    <div
      className={`keyboard keyboard-100
        keyset-${keyboardState.keyset}
        candybar-${keyboardState.background}`}
    >
      <FunctionRow />
      <TopNav />
      <section className=""></section>
      <SixtyPecent />
      <Navigation />
      <Numpad />
    </div>
  );
}
