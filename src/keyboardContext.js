import React, { useState, useMemo } from "react";

export const keyboardContext = React.createContext({ actions: {}, values: {} });

export default function KeyboardContextProvider({ children }) {
  const [keyboardState, setKeyboardState] = useState({
    background: "spacegrey",
    keyset: "drifter",
  });
  const [keycapColorState, setKeycapColorState] = useState({
    keysetName: "drifter",
    colorName: "teal",
  });

  const context = useMemo(() => {
    return {
      values: {
        keyboardState,
        keycapColorState,
      },
      actions: {
        setKeyboardState,
        setKeycapColorState,
      },
    };
  }, [keyboardState, keycapColorState]);

  return (
    <keyboardContext.Provider value={context}>
      {children}
    </keyboardContext.Provider>
  );
}
