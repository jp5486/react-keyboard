import React from "react";
import ChangeKeyBackground from "./changeKeyBackground";

const rowOne = ["/", "×", "←"];
const rowTwo = ["7", "8", "9", "+"];
const rowThree = ["4", "5", "6", "-"];
const rowFour = ["1", "2", "3"];
const rowFive = ["00", "0"];

export default function Numpad() {
  const formatList = (list, size) => {
    return list.map((value) => formatAnyKey(value, size));
  };

  const formatAnyKey = (value, size) => {
    switch (size) {
      case "1":
        return (
          <div onClick={ChangeKeyBackground("1")} key={value} className="key">
            <span className="legend">{value}</span>
          </div>
        );
      case "zero":
        return (
          <div
            onClick={ChangeKeyBackground("zero")}
            key={value}
            className="key zero"
          >
            <span className="legend">{value}</span>
          </div>
        );
      case "modifier":
        return (
          <div
            onClick={ChangeKeyBackground("nav")}
            key={value}
            className="key modifier"
          >
            <span className="legend">{value}</span>
          </div>
        );
      default:
        return;
    }
  };

  return (
    <section className="numpad">
      <div className="row row-1">
        {formatAnyKey("Num Lock", "modifier")}
        {formatList(rowOne, "1")}
      </div>
      <div className="row row-1">{formatList(rowTwo, "1")}</div>
      <div className="row row-2">{formatList(rowThree, "1")}</div>
      <div className="row row-3">
        {formatList(rowFour, "1")}
        {formatAnyKey("Enter", "modifier")}
      </div>
      <div className="row row-4">
        {formatList(rowFive, "zero")}
        {formatAnyKey(".", "1")}
        {formatAnyKey("Fn", "modifier")}
      </div>
    </section>
  );
}
