import React from "react";
import ChangeKeyBackground from "./changeKeyBackground";

const rowOne = ["Ins", "Home", "PgUp"];
const rowTwo = ["Del", "End", "PgDn"];
const rowFour = ["←", "↓", "→"];

export default function Navigation() {
  const formatList = (list, size) => {
    return list.map((value) => formatAnyKey(value, size));
  };

  const formatAnyKey = (value, size) => {
    return (
      <div
        onClick={ChangeKeyBackground("nav")}
        key={value}
        className="key modifier"
      >
        <span className="legend">{value}</span>
      </div>
    );
  };

  return (
    <section className="navigation-keys">
      <section className="row row-1">{formatList(rowOne, "nav")}</section>
      <section className="row row-2">{formatList(rowTwo, "nav")}</section>
      <section className="arrows">
        <section className="row row-4">
          <div
            onClick={ChangeKeyBackground("upArrow")}
            className="key modifier key-up"
          >
            <span className="legend">↑</span>
          </div>
        </section>
        <section className="row row-4">{formatList(rowFour, "nav")}</section>
      </section>
    </section>
  );
}
